{
    'name': 'Asignacion de almacenes para ordenes de meli',
    'version': '14.0.1.0.0',
    'category': 'stock',
    'summary': """Asignacion de almacenes para ordenes de meli""",
    'description': """
        En los metodos de envio de mercado libre se realizaran la asignacion de los almacenes de la siguiente manera:
        Asigna el mejor crum para las lineas de la orden de meli dependiento del tipo de envío que tiene
            * fulfillment:  Se asigna al almacen que cuenta con ese tipo de envio meli, es necesario solo un alamacen
            * cross_docking: se asigna por cercania de estados en almacen
            * self_service, default: se asigna por crums mas cercanos por cp
    """,
    'author': 'Prixz',
    'depends': ['stock_nearest_warehouse_acs', 'meli_oerp'],
    'data': [
        'security/ir.model.access.csv',
        'views/stock_warehouse_views.xml',
        'views/sale_order_views.xml',
    ],
    'license': "LGPL-3",
    'installable': True,
    'application': False,
}
