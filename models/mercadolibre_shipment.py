from odoo import api, models


class MercadolibreShipment(models.Model):
    _inherit = "mercadolibre.shipment"

    def _update_sale_order_shipping_info(self, order, meli=None, config=None):
        res = super(MercadolibreShipment, self)._update_sale_order_shipping_info(order, meli, config)
        for record in self:
            record.sale_order.set_best_crum_for_meli()
            morder = record.sale_order.meli_orders and record.sale_order.meli_orders[0]
            if morder:
                morder.update_order_status()
        return res
