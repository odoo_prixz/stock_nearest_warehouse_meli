from odoo import api, fields, models


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    def set_best_crum_for_meli(self):
        """
            Asigna el mejor crum para las lineas de la orden de meli dependiento del tipo de envío que tiene
            * fulfillment:  Se asigna al almacen que cuenta con ese tipo de envio meli, es necesario solo un alamacen
            * cross_docking: se asigna mediante cercania de estados en almacen
            * self_service, default: se asigna por crums mas cercanos por codigo
        """
        if self.state == "draft":
            if self.meli_shipment_logistic_type in ["fulfillment"]:
                self.order_line.set_meli_crum_full()
            if self.meli_shipment_logistic_type in ["cross_docking", "default"]:
                self.order_line.set_meli_crum_for_state()
            if self.meli_shipment_logistic_type in ["self_service"]:
                self.order_line.set_warehouse()


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    def set_meli_crum_full(self):
        for record in self:
            warehouse_id = self.env["stock.warehouse"].search([
                ("meli_shipment_logistic_type", "=", record.order_id.meli_shipment_logistic_type)
            ], limit=1)
            if warehouse_id:
                record.line_warehouse_id = warehouse_id.id

    def set_meli_crum_for_state(self):
        for record in self:
            record.line_warehouse_id = False
            if record.product_id and record.product_id.type in ['product', 'consu'] and record.state in ['draft', 'sent']:
                warehouse_id = record.get_warehouses_order_sate(partner_shipping_id=record.order_id.partner_shipping_id)
                if warehouse_id:
                    record.line_warehouse_id = warehouse_id.id

    def get_warehouses_order_sate(self, partner_shipping_id):
        """
            Regresa el almacen por stock prioridad cercania de estado
        """
        def _check_state_range_warehouse(state_ids):
            for state_id in state_ids:
                available_qty = self.env['stock.quant']._get_available_quantity(
                    self.product_id, state_id.warehouse_id.lot_stock_id, lot_id=False, strict=False)
                state_return = state_id.warehouse_id if available_qty and available_qty >= self.product_uom_qty else False
                if state_return:
                    return state_return
            return False

        def return_warehouse(state_ids):
            warehouse_id = _check_state_range_warehouse(state_ids)
            if warehouse_id:
                return warehouse_id

        if partner_shipping_id and partner_shipping_id.state_id:
            partner_state_id = partner_shipping_id.state_id
            state_ids = self.env['stock.state.range'].search([
                ('state_id', '=', partner_state_id.id),
                ('company_id', '=', self.company_id.id)
            ])
            state_ids = state_ids.sorted(lambda x: x.priority)

            if state_ids:
                warehouse_id = return_warehouse(state_ids)
                if warehouse_id:
                    return warehouse_id
                else:
                    return state_ids[0].warehouse_id
            else:
                return self.order_id.warehouse_id
        return False
