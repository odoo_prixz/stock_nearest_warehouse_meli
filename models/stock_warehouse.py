from odoo import fields, models, api, _


class StockWarehouse(models.Model):
    _inherit = "stock.warehouse"

    meli_shipment_logistic_type = fields.Selection([
        ('cross_docking', 'Colecta'),
        ('default', 'Flete'),
        ('self_service', 'Flex'),
        ('fulfillment', 'Full'),
    ], String='CRUM asignado para Meli')


